const fs = require("fs");

function getFootballClub() { // function create
    const footballclub = fs.readFileSync("./footballclub.json","utf-8");
    return JSON.parse(footballclub);
}

function addFootballClub() { // function add
    const footballclub = getFootballClub();
    footballclub.push(club);
    console.log("footballclub", footballclub);
    fs.writeFileSync("./footballclub.json", JSON.stringify(footballclub));
}

function deleteFootballClub(jersey) { // function delete
    const footballclub = getFootballClub();
    const footballclubFiltered = footballclub.filter((club) => {
        return club.jersey !== jersey;
    });
    console.log("footballclubFiltered", footballclubFiltered);
    fs.writeFileSync("./footballclub.json", JSON.stringify(footballclubFiltered));
}

function getDetailFootballClub(jersey) { // function get detail
    const footballclub = getFootballClub();
    footballclub.forEach((club) => {
        if (club.jersey === jersey) {
            console.log("club", club);
        }
    });
}

if (Number(process.argv[2]) == 1) {
    const footballclub = getFootballClub();
    console.log("footballclub", footballclub);
}

if (Number(process.argv[2]) == 2 ) {
    const club = {
        club: process.argv[3],
        newplayers: process.argv[4],
        jersey: process.argv[5],
        stadion: process.argv[6]
};
addFootballClub(club);
}

if (Number(process.argv[2]) == 3 ) {
    const footballclub = deleteFootballClub();
    console.log("footballclub", footballclub);
}

if (Number(process.argv[2]) == 4 ) {
    getDetailFootballClub(process.argv[3]);
}